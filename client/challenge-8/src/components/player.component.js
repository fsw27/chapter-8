import React, { Component } from "react";
import PlayerDataService from "../services/player.service";
import { withRouter } from '../common/with-router';

class Player extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.getPlayer = this.getPlayer.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updatePlayer = this.updatePlayer.bind(this);
    this.deletePlayer = this.deletePlayer.bind(this);

    this.state = {
      currentPlayer: {
        id: null,
        title: "",
        description: "",
        published: false
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getPlayer(this.props.router.params.id);
  }

  onChangeTitle(e) {
    const title = e.target.value;

    this.setState(function(prevState) {
      return {
        currentPlayer: {
          ...prevState.currentPlayer,
          title: title
        }
      };
    });
  }

  onChangeDescription(e) {
    const description = e.target.value;
    
    this.setState(prevState => ({
      currentPlayer: {
        ...prevState.currentPlayer,
        description: description
      }
    }));
  }

  getPlayer(id) {
    PlayerDataService.get(id)
      .then(response => {
        this.setState({
          currentPlayer: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentPlayer.id,
      title: this.state.currentPlayer.title,
      description: this.state.currentPlayer.description,
      published: status
    };

    PlayerDataService.update(this.state.currentPlayer.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentPlayer: {
            ...prevState.currentPlayer,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePlayer() {
    PlayerDataService.update(
      this.state.currentPlayer.id,
      this.state.currentPlayer
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The Player was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deletePlayer() {    
    PlayerDataService.delete(this.state.currentPlayer.id)
      .then(response => {
        console.log(response.data);
        this.props.router.navigate('/Players');
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentPlayer } = this.state;

    return (
      <div>
        {currentPlayer ? (
          <div className="edit-form">
            <h4>Player</h4>
            <form>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  className="form-control"
                  id="title"
                  value={currentPlayer.title}
                  onChange={this.onChangeTitle}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Description</label>
                <input
                  type="text"
                  className="form-control"
                  id="description"
                  value={currentPlayer.description}
                  onChange={this.onChangeDescription}
                />
              </div>

              <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentPlayer.published ? "Published" : "Pending"}
              </div>
            </form>

            {currentPlayer.published ? (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(true)}
              >
                Publish
              </button>
            )}

            <button
              className="badge badge-danger mr-2"
              onClick={this.deletePlayer}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updatePlayer}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Player...</p>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(Player);