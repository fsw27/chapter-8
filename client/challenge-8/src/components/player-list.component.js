import React, { Component } from "react";
import PlayerDataService from "../services/player.service";
import { Link } from "react-router-dom";

export default class PlayersList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrievePlayers = this.retrievePlayers.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActivePlayer = this.setActivePlayer.bind(this);
    this.removeAllPlayers = this.removeAllPlayers.bind(this);
    this.searchTitle = this.searchTitle.bind(this);

    this.state = {
      Players: [],
      currentPlayer: null,
      currentIndex: -1,
      searchTitle: ""
    };
  }

  componentDidMount() {
    this.retrievePlayers();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrievePlayers() {
    PlayerDataService.getAll()
      .then(response => {
        this.setState({
          Players: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrievePlayers();
    this.setState({
      currentPlayer: null,
      currentIndex: -1
    });
  }

  setActivePlayer(Player, index) {
    this.setState({
      currentPlayer: Player,
      currentIndex: index
    });
  }

  removeAllPlayers() {
    PlayerDataService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  searchTitle() {
    this.setState({
      currentPlayer: null,
      currentIndex: -1
    });

    PlayerDataService.findByTitle(this.state.searchTitle)
      .then(response => {
        this.setState({
          Players: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { searchTitle, Players, currentPlayer, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by title"
              value={searchTitle}
              onChange={this.onChangeSearchTitle}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.searchTitle}
              >
                Search
              </button>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <h4>Players List</h4>

          <ul className="list-group">
            {Players &&
              Players.map((Player, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActivePlayer(Player, index)}
                  key={index}
                >
                  {Player.title}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllPlayers}
          >
            Remove All
          </button>
        </div>
        <div className="col-md-6">
          {currentPlayer ? (
            <div>
              <h4>Player</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>{" "}
                {currentPlayer.title}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentPlayer.description}
              </div>
              <div>
                <label>
                  <strong>Status:</strong>
                </label>{" "}
                {currentPlayer.published ? "Published" : "Pending"}
              </div>

              <Link
                to={"/Players/" + currentPlayer.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Player...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
